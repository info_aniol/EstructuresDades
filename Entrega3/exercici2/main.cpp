
#include <bits/stdc++.h>

#include "BSTMovieFind.h"

using namespace std;

void mostrar_menu()
{
    cout << "1.- Crear i omplir un arbre" << endl;
    cout << "2.- Mostrar arbre" << endl;
    cout << "3.- Cercar películes de cercaPelicules.txt a l'arbre" << endl;
    cout << "4.- Mostrar alçada de l'arbre" << endl;
    cout << "5.- Sortir" << endl;
}

int main(int argc, char** argv) 
{

    
    BSTMovieFind* tree = nullptr;
    cout << "TEST DE BSTMovieFind" << endl;
    int opcio;
    do{
        cout << endl;
        mostrar_menu();
        cout << "Introdueixi l'opció a executar: ";
        cin >> opcio;
        while((opcio < 1 || opcio > 5) || (opcio != 1 && opcio != 5 && tree == nullptr))
        {
            cout << "Opció no vàlida, ";
            if(opcio < 1 || opcio > 5)
            {
                cout << "índex fora de rang." << endl;
            }
            else
            {
                cout << "encara no heu inicialitzat l'arbre." << endl;
            }
            cout << endl;
                    
            mostrar_menu();
            cin >> opcio;
        }

        switch(opcio)
        {
            case 1:
            {
                delete tree;
                tree = new BSTMovieFind();
                cout << "Quin fitxer voleu utilitzar per a les proves? [g/p]" << endl;
                string entrada;
                cin >> entrada;
                while(entrada != "g" && entrada != "p" && entrada != "G" && entrada != "P")
                {
                    cout << "Entrada no vàlida. Siusplau, introduïu 'g' o 'p'" << endl;
                    cin >> entrada;
                }

                cout << "Inicialitzant l'arbre amb el fitxer escollit..." << endl;
                double temps_inici;
                if(entrada == "g" || entrada == "G")
                {
                    clock_t start = clock();
                    tree->appendMovies("movie_rating.txt");
                    clock_t end = clock();
                    temps_inici = double(end - start);         
                }
                else
                {
                    clock_t start = clock();
                    tree->appendMovies("movie_rating_small.txt");
                    clock_t end = clock();
                    temps_inici = double(end - start);
                }

                cout << "Arbre incialitzat. Temps transcorregut: " << temps_inici << " ticks (uns " << temps_inici/CLOCKS_PER_SEC << " segons)." << endl << endl;
                break;
            }

            case 2:
            {        
                cout << "Mostrant películes en ordre creixent d'ID:" << endl;
                try
                {
                    tree->printSorted();
                }
                catch(exception &e)
                {
                    cout << e.what()  << endl;
                }
                break;
            }

            case 3:
            {
                cout << "Cercant películes de cercaPelicules.txt a l'arbre:" << endl;
                ifstream file("cercaPelicules.txt");
                int id, cont = 0;
                clock_t start = clock();
                while(file >> id)
                {
                    try
                    {
                        tree->findMovie(id);
                        cont++;
                    }
                    catch(exception &e)
                    {
                    //Aquí no hi podem fer res... Si find movies tornés una referència podríem comprovar si és null...    
                    }
                }
                clock_t end = clock();
                file.close();
                double temps_cerca = double(end - start);
                cout << "S'ha trobat un total de " << cont << " coincidències en un total de " << temps_cerca << " ticks (uns " << temps_cerca/CLOCKS_PER_SEC << " segons)." << endl;
                break;
            }

            case 4:
            {
                cout << "L'arbre té una alçada de " << tree->getHeight() << " nivells." << endl;
                break;
            }

            case 5:
            {
                delete tree;
                break;
            }
        }
    } while(opcio != 5);
}
