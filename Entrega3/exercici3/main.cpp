#include <iostream>

#include "BalancedBST.hpp"

using namespace std;

void mostrar_menu()
{
    cout << "0.- Executar test de prova" << endl;
    cout << "1.- Inserir element" << endl;
    cout << "2.- Imprimir Pre" << endl;
    cout << "3.- Imprimir Post" << endl;
    cout << "4.- Imprimir In" << endl;
    cout << "5.- Mostrar mida" << endl;
    cout << "6.- Eliminar i sortir" << endl;
}

void array_to_tree(int arr[], int size, BalancedBST<int> &tree)
{
    for(int i = 0; i < size; i++)
    {
        tree.insert(arr[i]);
        cout << "Inserint element " << arr[i] << " a l'arbre." << endl;
    }
}

void test_1()
{
    int arr[] = {2, 0, 8, 45, 76, 5, 3, 40};
    BalancedBST<int> *tree = new BalancedBST<int>();
    cout << "Arbre creat." << endl << endl;
    array_to_tree(arr,sizeof(arr)/sizeof(int), *tree);
    
    cout << "imprimit arbre en post-ordre:" << " [";
    tree->printPostorder();
    cout << "]" << endl << endl;
    
    cout << "imprimit arbre en pre-ordre:" << " [";
    tree->printPreorder();
    cout << "]" << endl << endl;
    
    cout << "imprimit arbre en in-ordre:" << " [";
    tree->printInorder();
    cout << "]" << endl << endl;
    
    BalancedBST<int> *nou = tree->mirror();
    cout << "Arbre mirall creat" << endl << endl;
    
    cout << "imprimit arbre mirall en pre-ordre:" << " [";
    nou->printPreorder();
    cout << "]" << endl << endl;
    delete nou;
    delete tree;
    cout << "Arbre binari destruït." << endl;
}   

/*
 * 
 */
int main(int argc, char** argv)
{
    BalancedBST<int> *arbre = new BalancedBST<int>();
    int opcio;
    do{
        cout << endl;
        mostrar_menu();
        cout << "Introdueixi l'opció a executar: ";
        cin >> opcio;
        while(opcio < 1 && opcio > 6)
        {
            cout << "Opció no vàlida." << endl;
            mostrar_menu();
            cin >> opcio;
        }
        
        switch(opcio)
        {
            
            case 0:
            {
                test_1();
                break;
            }
            case 1:
            {
                int value;
                cout << "Introduiexi el nombre que vol afegir:" << endl;
                cin >> value;
                try{
                    arbre->insert(value);
                    cout << "Nombre " << value << " afegit." << endl;
                }
                catch(exception &e)
                {
                    cout << e.what() << endl;
                }
                break;
            }
            
            case 2:
            {
                arbre->printPreorder();
                break;
            }
            
            case 3:
            {
                arbre->printPostorder();
                break;
            }
            
            case 4:
            {
                arbre->printInorder();
                break;
            }
            
            case 5:
            {
                cout << "Mida de l'arbre: " << arbre->size() << endl;
                break;
            }
            
            case 6:
            {
                delete arbre;
                return 0;
            }
        }
    } while(opcio != 6);
}


// -----------------------------------------
//    Preguntes 
// -----------------------------------------
/**
Cost computacional de BinarySearch Tree:
        BinarySearchTree() -> O(1), ja que només assigna nullptr a root;
        BinarySearchTree(const BinarySearchTree& orig); 0(n), ja que es fa un recorregut postOrdre (O(n)) amb operacions de O(1) pel mig.
        virtual ~BinarySearchTree(); O(n), ja que es criden n destructors de node, cada un de cost O(1)
        
        int size() const; O(n), ja que cridem la funció size de més avall, de cost O(n)
        bool isEmpty() const; O(1), només hem de comprovar un punter
        NodeTree<Type>* root(); o(1), retorna directament un punter
        bool search(const Type& element); O(height), veure funció següent
        bool searchNode(const Type &element, const NodeTree<Type> &Node); O(height), (height del subarbre amb arrel a Node) ja que com 
 *          a màxim passem un cop per cada nivell. Si l'arbre fos complet, seria equivalent a O(logn), el cost de la cerca binària tradicional
        void printInorder() const; O(n), ja que es visiten tots els nodes amb un cost d'O(1) cada un
        void printPreorder() const; O(n), ja que es visiten tots els nodes amb un cost d'O(1) cada un
        void printPostorder() const; O(n), ja que es visiten tots els nodes amb un cost d'O(1) cada un
        int getHeight(); Tal com s'ha implementat, O(1), ja que només cal consultar una variable que s'ha anaat modificant a l'insert amb cost O(1).
 *          S'hagués pogut fer una fucnió recursiva que calculés l'alçada màxima de cada node com el màxim de l'alçada màxima a partir dels seus fills +1
 *          però això tindria un cost de O(n), mentre que si aprofitem l'insert per anar-la actualitzant amb cost O(1), després només hem de cosnultar 
 *          una sola variable.
        

        void insert(const Type& element); O(n) ja que consisteix en una cerca (O(n)) + afegir node (O(1)) + actualitzar alçada màxima (O(1))
        BinarySearchTree<Type> *mirror(); O(n), ja que és pràcticament idèntic al costructor còpia, que ja hem dit que era d'O(n)
        
    private:
        
        NodeTree<Type>* recursive_copy(NodeTree<Type> *orig_node); O(n), amb n el nombre de nodes del subarbre amb arrel a orig_node 
 *          ja que es tracta d'un recorregut postordre per tots els nodes per sota de orig_node amb operacions auxiliars d'O(1)
        void postDelete(NodeTree<Type>* p); O(n), ja que es tracta d'un recorregut postordre amb operacions auxiliars d'O(1)
        int size(NodeTree<Type>* p) const;
        void printPreorder(NodeTree<Type>* p) const; O(n), ja que es visiten tots els nodes amb un cost d'O(1) cada un
        void printPostorder(NodeTree<Type>* p) const; O(n), ja que es visiten tots els nodes amb un cost d'O(1) cada un
        void printInorder(NodeTree<Type>* p) const; O(n), ja que es visiten tots els nodes amb un cost d'O(1) cada un
        int getHeight(NodeTree<Type>* p); O(1), ja que és simplement consultar una atribut de Node
        NodeTree<Type>* mirror_subtree(NodeTree<Type> *node); O(n) amb n el nombre de nodes del subarbre amb arrel a node
 *          ja que es tracta d'un recorregut postordre per tots els nodes per sota de node amb operacions auxiliars d'O(1)
 
 
 Cost computacional de NodeTree
 
   
        NodeTree(const Type& data); O(1), només s'assignen valors a variables i punters
        NodeTree(const NodeTree& orig); O(1), tal com el constructor, només s'assignen valors a atributs 
        virtual ~NodeTree(); O(1), ja que només s'eliminene dos punters
        
        NodeTree<Type>* getRight() const; O(1), ja que només es consulta i retorna un atribut
        NodeTree<Type>* getLeft() const; O(1), ja que només es consulta i retorna un atribut
        NodeTree<Type>* getParent() const; O(1), ja que només es consulta i retorna un atribut
        bool hasRight() const; O(1), ja que només es consulta  un atribut
        bool hasLeft() const; O(1), ja que només es consulta un atribut
        bool isRoot() const; O(1), ja que només es consulta un atribut
        bool isExternal() const; O(1), ja que només es consulta un atribut
        const Type& getElement() const; O(1), ja que només es consulta i retorna un atribut
        int getHeight() const; O(1), ja que només es consulta i retorna un atribut
        

        void setHeight(int h); O(1), ja que només es canvia el valor d'un atribut
        void setData(const Type& data); O(1), ja que només es canvia el valor d'un atribut
        void setRight(NodeTree<Type>* newRight); O(1), ja que només es canvia el valor d'un atribut
        void setLeft(NodeTree<Type>* newLeft); O(1), ja que només es canvia el valor d'un atribut
        void setParent(NodeTree<Type>* newParent); O(1), ja que només es canvia el valor d'un atribut
 */