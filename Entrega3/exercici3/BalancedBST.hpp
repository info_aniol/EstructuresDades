#ifndef BINARYSEARCHTREE_HPP
#define BINARYSEARCHTREE_HPP

#include "NodeTree.hpp"
#include <iostream>
#include <stdexcept>

using namespace std;


template <class Type>
class BalancedBST
{
    public:
        /*Constructors i Destructors*/
        BalancedBST();
        BalancedBST(const BalancedBST& orig);
        virtual ~BalancedBST();
        
        /*Consultors*/
        int size() const;
        bool isEmpty() const;
        NodeTree<Type>* root();
        Type search(const Type &id) const;
        void printSorted() const;
        void printInorder() const;
        void printPreorder() const;
        void printPostorder() const;
        int getHeight();
        
        /*Modificadors*/
        void insert(const Type& key);
        BalancedBST<Type> *mirror();
        
    private:
        
        NodeTree<Type>* recursive_copy(NodeTree<Type> *orig_node);
        void postDelete(NodeTree<Type>* p);
        int size(NodeTree<Type>* p) const;
        Type recursive_search(const Type &id, const NodeTree<Type> *Node) const;
        void printSorted(NodeTree<Type>* node, int &cont, bool &premature) const;
        void printPreorder(NodeTree<Type>* p) const;
        void printPostorder(NodeTree<Type>* p) const;
        void printInorder(NodeTree<Type>* p) const;
        int getHeight(NodeTree<Type>* p); 
        NodeTree<Type>* mirror_subtree(NodeTree<Type> *node);
        
        void rotLeft(NodeTree<Type>* root);
        void rotRight(NodeTree<Type>* root);
        void rotLeftRight(NodeTree<Type>* root);
        void rotRightLeft(NodeTree<Type>* root);
        
        /*Atributs*/
        NodeTree<Type>* pRoot;
};



// -----------------------------------------
//    Constructors i destructor
// -----------------------------------------

/**
 * Constructor per defecte de la classe BinarySearchTree
 */
template <class Type>
BalancedBST<Type>::BalancedBST()
{
    this->pRoot = nullptr;
}


/**
 * Constructor còpia de la classe BinarySearchTree
 * @param orig Objecte de la classe BinarySearchTree, corresponenet a l'arbre
 *  que es vol copiar
 */
template <class Type>
BalancedBST<Type>::BalancedBST(const BalancedBST& orig)
{
    this->pRoot = new NodeTree<Type>(orig.pRoot->getElement()); //definim el node arrel amb el mateix id que l'arrel de l'arbre original
    if(orig.pRoot->hasLeft()) //Si té fill esquerre
    {
        NodeTree<Type> *l_node = recursive_copy(orig.pRoot->getLeft()); //Generem recursivament el subarbre amb arrel al node esquerre
        this->pRoot->setLeft(l_node); //Indiquem que és el fill dret de l'arrel
        l_node->setParent(this->pRoot); //Indiquem que l'arrel és el pare del node que hem generat
    }
    if(orig.pRoot->hasRight()) //Anàlogament amb el subarbre dret
    {
        NodeTree<Type> *r_node = recursive_copy(orig.pRoot->getRight());
        this->pRoot->setRight(r_node);
        r_node->setParent(this->pRoot);
    }  
}

/**
 * Destructor de la classe BinarySearchTree
 */
template <class Type>
BalancedBST<Type>::~BalancedBST()
{
    //Es podria fer servir la funció postDelete(), però tal com hem implementat
    //  el destructor de la classe NodeTree, no és necessari. Tan sols caldrà
    //  que cridem el destructor del node arrel i aquest anirà cridant 
    //  recursivament els destructors de tots els nodes de l'arbre.
    
    
    //postDelete(this->pRoot);
    delete this->pRoot; 
    this->pRoot = nullptr;
}



// -----------------------------------------
//    Funcions consultores
// -----------------------------------------


/**
 * Funció per obtenir el nombre total de nodes de l'arbre
 * @return entrer corresponent al nombre total de nodes
 */
template <class Type>
int BalancedBST<Type>::size() const
{
    return size(this->pRoot);
}

/**
 * Funció per comprovar si l'arbre és buit o no
 * @return booleà indicant si està buit o no
 */
template <class Type>
bool BalancedBST<Type>::isEmpty() const
{
    //En el nostre cas, hem definit que l'arbre està buit si el node arrel és nul
    return (this->pRoot == nullptr);
}

/**
 * Funció per obtenir el node arrel
 * @return punter a NodeTree de l'arrel de l'arbre
 */
template <class Type>
NodeTree<Type>* BalancedBST<Type>::root()
{
    return this->pRoot;
}

/**
 * Fnució per comprovar si un id és a l'arbre
 * @param id referència a l'id que es vol cercar
 * @return boolèa indicant si l'id és a l'arbre o no
 */
template <class Type>
Type BalancedBST<Type>::search(const Type &id) const
{
    return recursive_search(id, this->pRoot);
}

/**
 * Mètode per imprimir l'arbre ordenadament i preguntant cada 40 elements
 */
template <class Type> 
void BalancedBST<Type>::printSorted() const
{
    if(this->isEmpty())
    {
        throw logic_error("L'arbre és buit!");
    }
    else
    {
        int cont = 1;
        bool premature = false;
        printSorted(this->pRoot, cont, premature);
    }    
}

/**
 * Mètode per imprimir en in-ordre el contingut de l'arbre
 */
template <class Type>
void BalancedBST<Type>::printInorder() const
{
    if(this->isEmpty())
    {
        throw logic_error("L'arbre és buit!");
    }
    else
    {
        printInorder(this->pRoot);
    }
}

/**
 * Mètode per imprimir en pre-ordre el contingut de l'arbre
 */
template <class Type>
void BalancedBST<Type>::printPreorder() const
{
    if(this->isEmpty())
    {
        throw logic_error("L'arbre és buit!");
    }
    else
    {
        printPreorder(this->pRoot);
    }
}

/**
 * Mètode per imprimir en post-ordre el contingut de l'arbre
 */
template <class Type>
void BalancedBST<Type>::printPostorder() const
{    
    if(this->isEmpty())
    {
        throw logic_error("L'arbre és buit!");
    }
    else
    {
        printPostorder(this->pRoot);
    }
}

/**
 * Mètode per obtenir l'alçada total de l'arbre
 * @return enter amb l'alçada
 */
template <class Type>
int BalancedBST<Type>::getHeight()
{
    return this->pRoot->getHeight();
}


// -----------------------------------------
//    Funcions modificadores
// -----------------------------------------


/**
 * Mètode per afegir un id a l'arbre
 * @param id id que es vol afegir
 */
template <class Type>
void BalancedBST<Type>:: insert(const Type& key) 
{
    //Si l'arbre era buit
    if(this->pRoot == nullptr)
    {
        NodeTree<Type> *root = new NodeTree<Type>(key); //Creem el node arrel amb l'id
        root->setHeight(0); //Indiquem que l'alçada de l'arrel és 0
        this->pRoot = root; //Indiquem que el node que hem creat és l'arrel de l'arbre
    }
    else
    {
        NodeTree<Type> *aux = this->pRoot;
        
        //Mentre el subarbre on s'ha de trobar el node existeixi
        while(not ((key < aux->getElement() && aux->getLeft() == nullptr) || (key > aux->getElement() && aux->getRight() == nullptr)))
        {
            if(key == aux->getElement())
            {
                throw invalid_argument("L'id ja és a l'arbre!");
            }
            
            if(key < aux->getElement())
            {
                aux = aux->getLeft();
            }
            else
            {
                aux = aux->getRight();
            }
        }
        
        NodeTree<Type> *nou = new NodeTree<Type>(key); //Creem el nou node amb l'id 
        nou->setParent(aux); //El pare del nou és la fulla que hem trobat amb la cerca
        if(key > aux->getElement()) //Comprovem si nou ha de ser fill esquerre o dret
        {
            aux->setRight(nou);
        }
        else
        {
            aux->setLeft(nou);
        }

        
        nou->setHeight(0); //Posem l'alçada i actualitzem tots els antecessors.
        
        //Anem pujant fins arribar a root, comprovant si està tot balancejat
        NodeTree<Type> *it = nou;
        while(it != nullptr)
        {
            if(it->getBalance() >= 2) //Si el node està desbalancejat pel fill esquerre
            {
                if(it->hasLeft() && it->getLeft()->getBalance() > 0) //Si l'error ve del subarbre esquerre del fill esquerre
                {
                    rotRight(it); //Amb una rotació simple és suficient per balancejar
                }
                else //Si ve del subarbre dert del fill esquerre
                { 
                    rotLeftRight(it); //Necessitem rotació doble
                }
            }
            else if(it->getBalance() <= -2) //Si el debalanceig ve del fill dret
            {
                if(it->hasRight() && it->getRight()->getBalance() > 0) // i del seu subarbre esquerre
                {
                    rotRightLeft(it);
                }
                else // si ve del subarbre dret
                {
                    rotLeft(it);
                }
            }
            it = it->getParent(); //Agafent el següent antecessor
        }
    }
}


/**
 * Mètode per fer l'arbre mirall
 * @return Retorna un punter de BinarySearchTree a un nou arbre mirall
 */
template<class Type>
BalancedBST<Type>* BalancedBST<Type>::mirror()
{
    BalancedBST<Type>* mirror = new BalancedBST();
    mirror->pRoot = new NodeTree<Type>(this->pRoot->getElement()); //definim el node arrel
    
    if(this->pRoot->hasLeft()) //Si té fill esquerre
    {
        NodeTree<Type> *l_node = mirror_subtree(this->pRoot->getLeft()); //Generem recursivament el subarbre amb arrel al node esquerre
        mirror->root()->setRight(l_node); //Però hi podem el fill dret
        l_node->setParent(mirror->pRoot);
    }
    
    if(this->pRoot->hasRight()) //Anàlogament amb el subarbre dret
    {
        NodeTree<Type> *r_node = mirror_subtree(this->pRoot->getRight());
        mirror->root()->setLeft(r_node);
        r_node->setParent(this->pRoot);
    }  
    return mirror;
}



// -----------------------------------------
//    Funcions auxiliars
// -----------------------------------------


/**
 * Mètode auxiliar per fer la còpia d'un arbre en preordre
 * @param orig_node punter de NodeTree a arrel de l'arbre a copiar
 * @return Punter de NodeTree a l'arrel de l'arbre que hem copiat
 */
template<class Type>
NodeTree<Type>* BalancedBST<Type>::recursive_copy(NodeTree<Type> *orig_node)
{
    Type id = orig_node->getElement(); //Agafem l'id del node original
    NodeTree<Type> *new_node = new NodeTree<Type>(id); //Creem un nou node amb aquest id
    if(orig_node->hasLeft()) //Comprovem si té fill esquerre
    {
        NodeTree<Type> *l_node = recursive_copy(orig_node->getLeft()); //Fem la còpia recursiva a partir del fill esquerre del node original
        new_node->setLeft(l_node); //El fill esquerre del nou node és l'l_node que acabem de generar
        l_node->setParent(new_node); //El pare d'l_node és el node nou
    }
    if(orig_node->hasRight()) //Anàlogament per la dreta
    {
        NodeTree<Type> *r_node = recursive_copy(orig_node->getRight());
        new_node->setRight(r_node);
        r_node->setParent(new_node);
    }
    return new_node; //Retornem el node principal, ara amb tots els seus fills
}

/**
 * Funció auxiliar per eliminar l'arbre recursivament
 * @param p Punter de NodeTree a l'arrel de l'arbre que es vol eliminar
 */
template <class Type>
void BalancedBST<Type>::postDelete(NodeTree<Type> *p)
{
    if(p->hasLeft())
    {
        postDelete(p->getLeft());
    }
    if(p->hasRight())
    {
        postDelete(p->getRight());
    }
    delete p;
}

/**
 * Funció auxiliar per determinar el nombre de nodes d'un arbre
 * @param node punter de NodeTree a l'arrel de l'arbre que es vol mesurar
 * @return enter amb el nombre d'ids de l'arbre.
 */
template <class Type>
int BalancedBST<Type>::size(NodeTree<Type> *node) const
{
    //El nombre de nodes d'un arbre és la suma del nombre de nodes dels subarbres amb arrel els fills de l'arrel de l'inicial
    if(node == nullptr) //Si hem anat més enllà de les fulles
    {
        return 0;
    }
    return 1 + size(node->getLeft()) + size(node->getRight());
}

/**
 * Mètode auxiliar per buscar ids a l'arbre
 * @param id referència a l'id que es vol cercar
 * @param node referència al node arrel del subarbre on efectuar la cerca
 * @return booleà indicant si l'id és a l'arbre o no
 */
template <class Type>
Type BalancedBST<Type>::recursive_search(const Type &id, const NodeTree<Type> *node) const
{
    //Si l'element del node eue ens han passat és el que busquem, retornem true
    if (node->getElement() == id) {
        return node->getElement();
    }

    //Si els subarbres on, segons les propietats del arbres de cerca binària s'hauria de trobar l'element, no existeixem
    if ((id < node->getElement() && node->getLeft() == nullptr) || (id > node->getElement() && node->getRight() == nullptr)) {
        throw invalid_argument("L'element que busqueu no és a l'arbre");
    }

    //Si existeixen, agafem el node arrel del que ens interessa i cridem recursivament aquesta funció
    if (id > node->getElement()) {
        return recursive_search(id, node->getRight());
    } else {
        return recursive_search(id, node->getLeft());
    }
}

template<class Type>
void BalancedBST<Type>::printSorted(NodeTree<Type> *node, int &cont, bool &premature) const 
{   
    if(!premature) //Si no s'ha de sortit
    {
        if(node->getLeft()) //Cridem recursivament sobre el fill esquere
        {
            printSorted(node->getLeft(), cont, premature);
        }
        if(!premature) // Si a la crida recursiva sobre l'esquerre no s'ha demanat sortir
        {
            std::cout << node->getElement() << endl; //NO PODEM ASSEGURAR QUE SIGUI POSSIBLE IMPRIMIR EL VALOR!!!
            cont++;
            if(cont==40)
            {
                cout << "Voleu continuar imprimint la llista? [S/n]" << endl;
                char entrada;
                cin >> entrada;
                if(entrada == 'n') //Si no volem continuar imprimint
                {
                    premature = true; //Activem el flag de sortida prematura
                }
                else
                { //Si volem continuar imprimint (opció per defecte) posem el contador a 0
                    cont = 1;
                }
            }
        }
        if(!premature && node->getRight())
        {
            printSorted(node->getRight(), cont, premature);
        }
    }
}

/**
 * Mètode auxiliar per imprimir un arbre en pre-ordre
 * @param node punter de NodeTree al node arrel de l'arbre que es vol imprimir
 */
template <class Type> 
void BalancedBST<Type>::printPreorder(NodeTree<Type> *node) const
{
    std::cout << node->getElement() << " ";
    if(node->hasLeft())
    {
        printPreorder(node->getLeft());
    }
    if(node->hasRight())
    {
        printPreorder(node->getRight());
    }
}

/**
 * Mètode auxiliar per imprimir un arbre en post-ordre
 * @param node punter de NodeTree al node arrel de l'arbre que es vol imprimir
 */
template <class Type> 
void BalancedBST<Type>::printPostorder(NodeTree<Type> *node) const
{
    if(node->hasLeft())
    {
        printPostorder(node->getLeft());
    }
    if(node->hasRight())
    {
        printPostorder(node->getRight());
    }
    std::cout << node->getElement() << " ";
}

/**
 * Mètode auxiliar per imprimir un arbre en in-ordre
 * @param node punter de NodeTree al node arrel de l'arbre que es vol imprimir
 */
template <class Type> 
void BalancedBST<Type>::printInorder(NodeTree<Type> *node) const
{
    if(node->hasLeft())
    {
        printInorder(node->getLeft());
    }
    std::cout << node->getElement() << " ";
    if(node->hasRight())
    {
        printInorder(node->getRight());
    }
}

/**
 * Mètode auxiliar per obtenir l'alçada d'un node
 * @param node punter de NodeTree al node del qual es vol saber l'alçada
 */
template<class Type>
int BalancedBST<Type>::getHeight(NodeTree<Type>* node)
{
    return node->getHeight();
    
}

/**
 * Mètode auxiliar per crear l'arbre mirall
 * @param node
 */
template<class Type>
NodeTree<Type>* BalancedBST<Type>::mirror_subtree(NodeTree<Type> *node)
{
    Type id = node->getElement(); //Agafem l'id del node original
    NodeTree<Type> *new_node = new NodeTree<Type>(id); //Creem un nou node amb aquest id
    if(node->hasLeft()) //Comprovem si té fill esquerre
    {
        NodeTree<Type> *l_node = mirror_subtree(node->getLeft()); //Fem la còpia recursiva a partir del fill esquerre del node original
        new_node->setRight(l_node); //Ara volem que el fill dret sigui el subarbre esquerre
        l_node->setParent(new_node); //El pare d'l_node és el node nou
    }
    if(node->hasRight()) //Anàlogament per la dreta
    {
        NodeTree<Type> *r_node = mirror_subtree(node->getRight());
        new_node->setLeft(r_node);
        r_node->setParent(new_node);
    }
    return new_node; //Retornem el node principal, ara amb tots els seus fills
}


/**
 * Mètode per rotar a la dreta
 * @param root punter de NodeTree al node desbalancejat
 */
template <class Type>
void BalancedBST<Type>::rotRight(NodeTree<Type>* root)
{
    //Root és l'arrel del subarbre que volem girar
    NodeTree<Type>* pivot = root->getLeft(); //Pivot és el fill esquerre de l'arrel del subarbre a balancejar
    NodeTree<Type>* tmp = pivot->getRight();
    pivot->setRight(root);
    if(!root->isRoot()) //Si root té pare
    {
        //Comprovem si és fill esquerre o fill dret i el canviem per pivot
        if(root->getParent()->getLeft() == root)
        {
            root->getParent()->setLeft(pivot);
        }
        else
        {
            root->getParent()->setRight(pivot);
        }
    }
    else //En cas contrari, pivot serà el nou pRoot (root de l'arbre general)
    {
        this->pRoot = pivot;
    }
    pivot->setParent(root->getParent()); //Actualitzem el pare de pivot com el pare de root
    root->setParent(pivot); //Pivot és el pare de root
    root->setLeft(tmp); //El fill esquerre de pivot passa a ser el fill dret de root 
    if(tmp != nullptr) //Si tmp existeix (és un node i no nullptr)
    {
        tmp->setParent(root); //Assignem a root com a pare.
        tmp->update(); //Actualitzem les alçades de tmp i els seus antecessors
    }
    else
    {
        root->update(); //Si tmp no existeix, el node més baix sobre el que podem actualitzar és root.
    }
}

/**
 * Mètode per executar rotació simple a l'esquerra
 * @param root punter de NodeTree al node desbalancejat
 */
template <class Type>
void BalancedBST<Type>::rotLeft(NodeTree<Type>* root)
{
    //Prcedim de manera simètrica a rotLeft()
    NodeTree<Type>* pivot = root->getRight(); 
    NodeTree<Type>* tmp = pivot->getLeft();
    pivot->setLeft(root);
    if(!root->isRoot())
    {
        if(root->getParent()->getLeft() == root)
        {
            root->getParent()->setLeft(pivot);
        }
        else
        {
            root->getParent()->setRight(pivot);
        }
    }
    else
    {
        this->pRoot = pivot;
    }
    pivot->setParent(root->getParent());
    root->setParent(pivot);
    root->setRight(tmp);
    if(tmp != nullptr)
    {
        tmp->setParent(root);
        tmp->update();
    }
    else
    {
        root->update();
    }
}

/**
 * Mètode per executar rotació doble esquerra-dreta
 * @param root punter de NodeTree al node desbalancejat
 */
template <class Type>
void BalancedBST<Type>::rotLeftRight(NodeTree<Type>* root)
{
    rotLeft(root->getLeft());
    rotRight(root);
}

/**
 * Mètode per executar rotació doble dreta-esquerra
 * @param root punter de NodeTree al node desbalancejat
 */
template <class Type>
void BalancedBST<Type>::rotRightLeft(NodeTree<Type>* root)
{
    rotRight(root->getRight());
    rotLeft(root);
}

#endif /* BINARYSEARCHTREE_HPP */

