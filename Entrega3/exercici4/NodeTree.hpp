#ifndef NODETREE_HPP
#define NODETREE_HPP

#include <iostream>
using namespace std;

template <class Type>
class NodeTree
{
    public:
        /*Constructors i destructor*/
        NodeTree(const int& key, const Type& data); 
        NodeTree(const NodeTree& orig); 
        virtual ~NodeTree();
        
        /*Consultors*/
        NodeTree<Type>* getRight() const;
        NodeTree<Type>* getLeft() const;
        NodeTree<Type>* getParent() const;
        bool hasRight() const;
        bool hasLeft() const;
        bool isRoot() const;
        bool isExternal() const;
        const Type& getValue() const;
        const int getKey() const;
        int getHeight() const;
        int getBalance() const;
        
        /*Modificadors*/
        void setHeight(int h);
        void update();
        void setValue(const Type& data);
        void setKey(const int& key);
        void setRight(NodeTree<Type>* newRight);
        void setLeft(NodeTree<Type>* newLeft);
        void setParent(NodeTree<Type>* newParent);
        
    private:
        NodeTree<Type>* pParent;
        NodeTree<Type>* pLeft;
        NodeTree<Type>* pRight;
        int key;
        Type value;
        int height; 
}; 


// -----------------------------------------
//    Constructors i destructors
// -----------------------------------------


/**
 * Constructor de la classe NodeTree
 * @param data l'element que volem que contingui el node
 */
template <class Type> 
NodeTree<Type>::NodeTree(const int& key, const Type& data)
{
    this->pParent = nullptr;
    this->pLeft = nullptr;
    this->pRight = nullptr;
    this->value = data;
    this->key = key;
    this->setHeight(0);
}

/**
 * Cosntructor còpia de la classe NodeTree
 * @param orig node original a copiar
 */
template <class Type> 
NodeTree<Type>:: NodeTree(const NodeTree& orig)
{
    this->pParent = orig.getParent();
    this->pLeft = orig.getLeft();
    this->pRight = orig.getRight();
    this->value = orig.getValue();
    this->key = orig.getKey();
    this->height = orig.getHeight();
}

/**
 * Destructor de la classe NodeTree
 */
template <class Type> 
NodeTree <Type>:: ~NodeTree()
{
    delete this->pLeft;
    delete this->pRight;
    this->pLeft = nullptr;
    this->pRight = nullptr;
    this->pParent = nullptr;
}


// -----------------------------------------
//    Funcions consultores
// -----------------------------------------


/**
 * Getter del node fill esquerre
 * @return punter de NodeTree al fill esquerre
 */
template <class Type> 
NodeTree<Type>* NodeTree <Type>:: getLeft() const
{
    return this->pLeft;
}

/**
 * Getter del node fill dret
 * @return punter de NodeTree al fill dret
 */
template <class Type> 
NodeTree<Type>* NodeTree <Type>:: getRight() const
{
    return this->pRight;
}

/**
 * Getter del node pare
 * @return punter de NodeTree al pare
 */
template <class Type> 
NodeTree<Type>* NodeTree <Type>:: getParent() const
{
    return this->pParent;
}

/**
 * Mètode per comprovar si el node té fill esquerre
 * @return booleà segons si té fill esquerre o no
 */
template <class Type> 
bool NodeTree <Type>:: hasLeft() const
{
    return (pLeft!=nullptr);
}

/**
 * Mètode per comprovar si el node té fill dret
 * @return booleà segons si té fill dret o no
 */
template <class Type> 
bool NodeTree <Type>:: hasRight() const
{
    return (pRight!=nullptr);
}

/**
 * Mètode per comprovar si el node és l'arrel d'un arbre
 * @return booleà segons si és arrel o no
 */
template <class Type>
bool NodeTree <Type>:: isRoot() const
{
    return (pParent == nullptr);
}

/**
 * Mètode per comprovar si el node és una fulla
 * @return booleà segons si és fulla o no
 */
template <class Type>
bool NodeTree <Type>:: isExternal() const
{
    return (!this->hasRight() and !this->hasLeft());
}

/**
 * Getter de l'element que guarda el node
 * @return element del node
 */
template <class Type>
const Type& NodeTree <Type>:: getValue() const
{
    return this->value;
}

/**
 * Getter de la clau del node
 * @return enter amb la clau
 */
template <class Type>
const int NodeTree<Type>::getKey() const
{
    return this->key;
}

/**
 * Getter de l'alçada del node
 * @return enter amb l'alçada del node
 */
template <class Type>
int NodeTree <Type>:: getHeight() const
{
    return this->height;
}

/**
 * Getter del balance factor del node
 * @return enter amb el balance factor
 */
template <class Type>
int NodeTree<Type>::getBalance() const
{
    if(this->hasLeft() && this->hasRight())
    {
        return (this->getLeft()->getHeight())-(this->getRight()->getHeight());
    }
    else if(this->hasLeft())
    {
        return this->getLeft()->getHeight() + 1;
    }
    else if(this->hasRight())
    {
        return -1-this->getRight()->getHeight();
    }
    else
    {
        return 0;
    }
}

// -----------------------------------------
//    Funcions modificadores
// -----------------------------------------


/**
 * Setter de l'alçada del node i actualització dels antecessors
 * @param h enter corresponent a l'alçada del node
 */
template <class Type>
void NodeTree <Type>:: setHeight(int h)
{
    this->height = h;
    if(!this->isRoot())
    {
        if(this->getParent()->hasLeft() && this->getParent()->hasRight())
        {
            this->getParent()->setHeight(std::max(this->getParent()->getLeft()->getHeight(), this->getParent()->getRight()->getHeight())+1);
        }
        else if(this->getParent()->hasLeft())
        {
            this->getParent()->setHeight(this->getParent()->getLeft()->getHeight()+1);
        }
        else if(this->getParent()->hasRight())
        {
            this->getParent()->setHeight(this->getParent()->getRight()->getHeight()+1);
        }
        else
        {
            this->getParent()->setHeight(0);
        }
    }
}

/**
 * Mètode per actualitzar l'alçada d'un node i la dels seus antecessors
 */
template <class Type>
void NodeTree<Type>::update()
{
    if(this->hasRight() && this->hasLeft())
    {
        this->setHeight(max(this->getLeft()->getHeight(), this->getRight()->getHeight())+1);
    }
    else if(this->hasRight())
    {
        this->setHeight(this->getRight()->getHeight()+1);
    }
    else if(this->hasLeft())
    {
        this->setHeight(this->getLeft()->getHeight()+1);
    }
    else
    {
        this->setHeight(0);
    }
}

/**
 * Setter de l'element que emmagatzema el node
 * @param data element que conté el node
 */
template <class Type>
void NodeTree <Type>::setValue(const Type& data)
{
    this->value = data;
}

/**
 * Setter de la clau del node
 * @param key enter amb la clau
 */
template <class Type>
void NodeTree<Type>::setKey(const int& key)
{
    this->key = key;
}

/**
 * Setter del fill esquerre
 * @param newLeft punter de NodeTree al nou fill esquerre
 */
template <class Type>
void NodeTree <Type>::setLeft(NodeTree<Type>* newLeft)
{
    this->pLeft = newLeft;
}

/**
 * Setter del fill dret
 * @param newRight punter de NodeTree al nou fill dret
 */
template <class Type>
void NodeTree<Type>::setRight(NodeTree<Type>* newRight)
{
    this->pRight = newRight;
}

/**
 * Setter del pare
 * @param newParent punter de NodeTree al nou pare
 */
template <class Type>
void NodeTree<Type>::setParent(NodeTree<Type>* newParent)
{
    this->pParent = newParent;
}

#endif