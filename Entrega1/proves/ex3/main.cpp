#include <bits/stdc++.h>
#include "circle.h"

using namespace std;

int  menu();
void create_circle(int & cont);

int main()
{

    int opt;
    int cont = 1;
    
    while(1)
    {
        cout << "Hola, què vols fer?" << endl;
        opt = menu();
    
        switch(opt)
        {
            case 1:
                return 0;
            case 2:
                try
                {
                    create_circle(cont);
                }
                catch(int e)
                {
                    cout << "S'ha produït una excepció" << endl;
                }
        }
    }
}

int  menu()
{
    int opt;

    vector<string> vec_options = {"1. Sortir", "2. Introduir cercle"};
    do{

        for(int i = 0; i < vec_options.size(); i++)
        {
            cout << vec_options[i] << endl;
        }
        cin >> opt;
    }
    while(opt <= 0 || opt > vec_options.size());

    return opt;
}

void create_circle(int &n)
{

    float radius;

    cout << "Cercle número " << n << " Introdueixi el radi del cercle:" << endl;
    cin >> radius;

    if(radius <= 0)
    {
        cout << "Atenció: valor no vàlid. El radi ha de ser estrictament positiu" << endl;
        throw 0;
    }
    else
    {
        circle cercle(radius);
        cout << "L'àrea d'aqest cercle és de " << cercle.area() << endl;
        n++;
    }
}



