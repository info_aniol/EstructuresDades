#include <bits/stdc++.h>
#include "circle.h"

//_________________________________________________________________________________________________
//CONSTRUCTORS AND DESTRUCTORD

circle::circle() //Default class constructor
{
    this->radius = 0;
}

circle::circle(float radius)
{
    this->radius = radius;
}

circle::circle(circle& original)
{
    this->radius = original.radius;
}

circle::~circle()
{
}


//_________________________________________________________________________________________________

float circle::get_radius()
{
    return this->radius;
}

void circle::set_radius(float radius)
{
    this->radius = radius;
}

//_________________________________________________________________________________________________


float circle::area()
{
    return 3.141592*this->radius*this->radius;
}



