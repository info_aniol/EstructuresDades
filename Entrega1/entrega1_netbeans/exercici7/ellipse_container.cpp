#include "ellipse_container.h"

using namespace std;

ellipse_container::ellipse_container()
{
}

ellipse_container::ellipse_container(const ellipse_container& orig)
{
    this->container = orig.container;
}

ellipse_container::~ellipse_container()
{
    for(auto it = this->container.begin(); it != this->container.end(); ++it)
    {
        delete (*it);
    }    
}

void ellipse_container::add_ellipse(ellipse* elipse)
{
    this->container.push_back(elipse);
}

float ellipse_container::get_areas()
{
    float sum = 0;
    for(auto it = this->container.begin(); it != this->container.end(); ++it)
    {
        sum += (*it)->area();
    }
    return sum;
}
