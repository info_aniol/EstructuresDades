#include <bits/stdc++.h>

using namespace std;

int  menu(const vector<string> &vec_options); //Prototip de la funció menu

int main()
{
    string nom;
    string arr_options[] = {"1. Sortir", "2. Benvinguda", "3. Redefinir nom"};
    vector<string> vec_options(arr_options, arr_options +3);

    
    /*
    //Alternativa per passar l'array a vector
    for(int i = 0; i < 3; i++) 
    {
        vec_options.push_back(arr_options[i]);
    }*/

    
    int opt;

    cout << "Hola, com et dius?" << endl;
    cin >> nom;
    
    while(1)
    {
        cout << "Hola, " << nom << ". Què vols fer?" << endl;
        opt = menu(vec_options);
    
        switch(opt)
        {
            case 1:
                return 0;
            case 2:
                cout << "Benvingut/da a l'assignatura d'estructura de dades, " << nom << " ." << endl;
                break;
            case 3:
                cout << "Introduiex el nom nou: " << endl;
                cin >> nom;
                break;
        }
    }
}

int  menu(const vector<string> &vec_options) //Li passem una referència amb const per estalviar memòria
{
    int opt;
    bool entrada_correcta;
    
    
    do{
        
        entrada_correcta = true; //Suposarem inicialment que l'entrada és correcta
        
        for(int i = 0; i < vec_options.size(); i++) //Mostrem opcions
        {
            cout << vec_options[i] << endl;
        }
        
        cin >> opt; //Agafem entrada de l'usuari
        
        
        if(cin.fail()) //Si l'entrada no és del tipus desitjat
        {
            entrada_correcta = false; //L'entrada no és correcta
            cin.clear(); //Esborrem el buffer de cin
            cin.ignore(); //Diem que ignori l'entrada
            cout << "Heu d'introduir un enter!" << endl;
        }
        else if(opt <= 0 || opt > vec_options.size()) //Si és un enter però fora de rang
        {
            entrada_correcta = false; //L'entrada no és vàlida
            cout << "Opció fora de rang!" << endl;
        }
       
    }
    while(!entrada_correcta);
    
    return opt;
}
