
#include <bits/stdc++.h>

#include "HeapMovieFinder.h"

using namespace std;

void mostrar_menu()
{
    cout << "1.- Crear i omplir un heap" << endl;
    cout << "2.- Mostrar heap" << endl;
    cout << "3.- Cercar películes de cercaPelicules.txt al heap" << endl;
    cout << "4.- Mostrar alçada del heap" << endl;
    cout << "5.- Sortir" << endl;
}

int main(int argc, char** argv) 
{
    
    HeapMovieFinder* heap = nullptr;
    cout << "TEST DE HeapMovieFInder" << endl;
    int opcio;
    do{
        cout << endl;
        mostrar_menu();
        cout << "Introdueixi l'opció a executar: ";
        cin >> opcio;
        while((opcio < 1 || opcio > 5) || (opcio != 1 && opcio != 5 && heap == nullptr))
        {
            cout << "Opció no vàlida, ";
            if(opcio < 1 || opcio > 7)
            {
                cout << "índex fora de rang." << endl;
            }
            else
            {
                cout << "encara no heu inicialitzat el heap." << endl;
            }
            cout << endl;
                    
            mostrar_menu();
            cin >> opcio;
        }

        switch(opcio)
        {
            case 1:
            {
                delete heap;
                heap = new HeapMovieFinder();
                cout << "Quin fitxer voleu utilitzar per a les proves? [g/p]" << endl;
                string entrada;
                cin >> entrada;
                while(entrada != "g" && entrada != "p" && entrada != "G" && entrada != "P")
                {
                    cout << "Entrada no vàlida. Siusplau, introduïu 'g' o 'p'" << endl;
                    cin >> entrada;
                }

                cout << "Inicialitzant el heap amb el fitxer escollit..." << endl;
                double temps_inici;
                if(entrada == "g" || entrada == "G")
                {
                    clock_t start = clock();
                    heap->appendMovies("movie_rating.txt");
                    clock_t end = clock();
                    temps_inici = double(end - start);         
                }
                else
                {
                    clock_t start = clock();
                    heap->appendMovies("movie_rating_small.txt");
                    clock_t end = clock();
                    temps_inici = double(end - start);
                }

                cout << "Arbre incialitzat. Temps transcorregut: " << temps_inici << " ticks (uns " << temps_inici/CLOCKS_PER_SEC << " segons)." << endl << endl;
                break;
            }

            case 2:
            {        
                cout << "Mostrant películes en ordre creixent d'ID:" << endl;
                try
                {
                    heap->print();
                }
                catch(exception &e)
                {
                    cout << e.what()  << endl;
                }
                break;
            }

            case 3:
            {
                cout << "Cercant películes de cercaPelicules.txt a el heap:" << endl;
                ifstream file("cercaPelicules.txt");
                int id, cont = 0;
                clock_t start = clock();
                while(file >> id)
                {
                    try
                    {
                        heap->findMovie(id);
                        cont++;
                    }
                    catch(exception &e)
                    {
                    //Aquí no hi podem fer res... Si find movies tornés una referència podríem comprovar si és null...    
                    }
                }
                clock_t end = clock();
                file.close();
                double temps_cerca = double(end - start);
                cout << "S'ha trobat un total de " << cont << " coincidències en un total de " << temps_cerca << " ticks (uns " << temps_cerca/CLOCKS_PER_SEC << " segons)." << endl;
                break;
            }
            

            case 4:
            {
                cout << "L'arbre té una alçada de " << heap->getHeight() << " nivells." << endl;
                break;
            }

            case 5:
            {
                delete heap;
                break;
            }
        }
    } while(opcio != 5);
}
