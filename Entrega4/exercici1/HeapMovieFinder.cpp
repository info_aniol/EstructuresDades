/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   HeapMovieFinder.cpp
 * Author: aniol
 * 
 * Created on 24 / de maig / 2018, 00:33
 */

#include "HeapMovieFinder.h"

HeapMovieFinder::HeapMovieFinder() 
{
}


HeapMovieFinder::~HeapMovieFinder() 
{
}

void HeapMovieFinder::insertMovie(int id, string title, float rating)
{
    Movie mov(id, title, rating);
    Node<Movie> node(id, mov);
    this->heap.insert(node);
    
}

void HeapMovieFinder::appendMovies(string filename)
{
    std::ifstream file(filename);
    string movie;
    while(getline(file, movie)) //Anem agafant línies
    {
        string id_string, title, rating_string; 
        int id;
        float rating;        
        int end = movie.find("::"); //Busquem el primer separador
        id_string = movie.substr(0, end); //Guardem la ID
        movie.erase(0, end + 2); //Esborrem la part que ja no volem llegir
        end = movie.find("::"); //Busquem el segon separador
        title = movie.substr(0, end); //Guardem el títol
        movie.erase(0, end+2); //Esborrem el títol i el separador
        rating_string = movie; //El que queda és la puntuació
        
        istringstream ss(id_string); //Passem la ID a enter
        ss >> id;
        istringstream ss2(rating_string); //Passem la puntuació a float
        ss2 >> rating;
        
        try
        {
            insertMovie(id, title, rating); //Inserim pel·lícula
        }
        catch(exception &e)
        {
            cout << e.what() << endl;
        }
    }
}

string HeapMovieFinder::showMovie(int id)
{
    return this->heap.search(id).to_string();
}

void HeapMovieFinder::findMovie(int id)
{
    cout << this->heap.search(id) << endl;
}

void HeapMovieFinder::print()
{
    this->heap.printHeap();
}

int HeapMovieFinder::getHeight()
{
    this->heap.getHeight();
}
