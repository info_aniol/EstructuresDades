/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MinHeap.hpp
 * Author: mherralo19.alumnes
 *
 * Created on 14 / de maig / 2018, 13:12
 */

#ifndef MINHEAP_HPP
#define MINHEAP_HPP

#include <iostream>
#include <vector>
#include "Node.hpp"
#include <cmath>
using namespace std;

template <typename Type>
class MinHeap
{
    public:
        /*Constructors i destructor*/
        MinHeap();
        MinHeap(MinHeap& orig);
        virtual ~MinHeap();

        /*Consultors*/
        int size();
        bool empty();
        const int min();
        const Type minValues();
        void printHeap();
        const Type search(int key);
        int minChild(int pos);
        int getHeight();
        
        /*Modificadors*/
        void insert(Node<Type> &element);
        Type removeMin();
        
        
    private:
        vector<Node<Type> > heap;
};

// -----------------------------------------
//    Constructors i destructors
// -----------------------------------------

/**
 * Constructor de la classe MinHeap
 */
template <typename Type>
MinHeap<Type>::MinHeap()
{
}

/**
 * Cosntructor còpia de la classe MinHeap
 * @param orig MinHeap original a copiar
 */
template <typename Type>
MinHeap<Type>::MinHeap(MinHeap& orig)
{
    this->heap = orig.heap;
}

/**
 * Destructor de la classe MinHeap
 */
template <typename Type>
MinHeap<Type>::~MinHeap()
{
}

// -----------------------------------------
//    Funcions consultores
// -----------------------------------------

/**
 * Getter de size de MinHeap
 * @return enter amb la mida del vector heap
 */
template <typename Type>
int MinHeap<Type>::size()
{
    return this->heap.size();
}

/**
 * Getter per a saber si el heap és buit
 * @return boleà segons si la mida és zero o no
 */
template <typename Type>
bool MinHeap<Type>::empty()
{
    return this->size() == 0;
}

/**
 * Getter de la clau de l'element mínim del heap.
 * @return un enter amb la clau de l'element 
 * situat en la primera posició del vector
 */
template <typename Type>
const int MinHeap<Type>::min()
{
    if(!this->empty)
    {
        return this->heap[0].getKey();
    }
    throw logic_error("El heap és buit.");
}

/**
 * Getter del valor de l'element mínim del heap
 * @return un objecte Type amb el valor de l'element
 * situat en la primera posició del vector
 */
template <typename Type>
const Type MinHeap<Type>::minValues()
{
    if(!this->empty())
    {
        return this->heap[0].getValue();
    }
    throw logic_error("El heap és buit.");
}

template <typename Type>
void MinHeap<Type>::printHeap()
{
    cout << "[ ";
    while(!this->empty())
    {
        cout << removeMin().get_id() << " ";
    }
    cout << "]" << endl;
    
}

template <typename Type>
const Type MinHeap<Type>::search(int key)
{
    for(int i = 0; i < this->heap.size(); i++)
    {
        if(heap[i].getKey() == key)
        {
            return heap[i].getValue();
        }
    }
    throw invalid_argument("L'element no és al heap.");
}

template <typename Type>
int MinHeap<Type>::getHeight()
{
    if(this->empty())
    {
        return 0;
    }
    return floor(log(this->heap.size())/log(2)) + 1;
}

template <typename Type>
void MinHeap<Type>::insert(Node<Type> &element)
{
    this->heap.push_back(element);
    
    int last = this->heap.size()-1;
    while(heap[last].getKey() < heap[floor((last-1)/2)].getKey())
    {
        swap(heap[last], heap[floor((last-1)/2)]);
        last = floor((last-1)/2);
    }
}

template <typename Type>
Type MinHeap<Type>::removeMin()
{
    Type min_node = heap[0].getValue();
    int first = 0;
    swap(heap[first], heap[heap.size()-1]);
    while(floor(log(first)/log(2) )|| (heap[first].getKey() >= heap[2*first +1].getKey() || heap[first].getKey() >= heap[2*(first+1)].getKey()))
    {
        int min = this->minChild(first);
        swap(heap[first], heap[min]);
        first = min;
    }
    return min_node;
}

template <typename Type>
int MinHeap<Type>::minChild(int pos)
{
    if(heap[2*pos+1].getKey() > heap[2*pos+2].getKey())
    {
        return 2*pos+2;
    }
    else
    {
        return 2*pos+1;
    }
}
#endif /* MINHEAP_HPP */

