
#ifndef NODE_HPP
#define NODE_HPP

#include <iostream>
using namespace std;

template <class Type>
class Node
{
    public:
        
        /*Constructors i destructor*/
        Node(const int& key, const Type& data); 
        Node(const Node& orig); 
        virtual ~Node();
        
        /*Consultors*/
        const Type& getValue() const;
        const int getKey() const;
        
        /*Modificadors*/
        void setValue(const Type& data);
        void setKey(const int& key);
        
    private:
        
        int key;
        Type value;
            
};

// -----------------------------------------
//    Constructors i destructors
// -----------------------------------------


/**
 * Constructor de la classe Node
 * @param data l'element que volem que contingui el node
 */
template <class Type> 
Node<Type>::Node(const int& key, const Type& data)
{
    this->value = data;
    this->key = key;
}

/**
 * Cosntructor còpia de la classe Node
 * @param orig node original a copiar
 */
template <class Type> 
Node<Type>:: Node(const Node& orig)
{
    this->value = orig.getValue();
    this->key = orig.getKey();
}

/**
 * Destructor de la classe Node
 */
template <class Type> 
Node <Type>:: ~Node()
{
}

// -----------------------------------------
//    Funcions consultores
// -----------------------------------------

/**
 * Getter de l'element que guarda el node
 * @return element del node
 */
template <class Type>
const Type& Node <Type>:: getValue() const
{
    return this->value;
}

/**
 * Getter de la clau del node
 * @return enter amb la clau
 */
template <class Type>
const int Node<Type>::getKey() const
{
    return this->key;
}

// -----------------------------------------
//    Funcions modificadores
// -----------------------------------------

/**
 * Setter de l'element que emmagatzema el node
 * @param data element que conté el node
 */
template <class Type>
void Node <Type>::setValue(const Type& data)
{
    this->value = data;
}

/**
 * Setter de la clau del node
 * @param key enter amb la clau
 */
template <class Type>
void Node<Type>::setKey(const int& key)
{
    this->key = key;
}

#endif /* NODE_HPP */