#ifndef ARRAYDEQUE_H
#define ARRAYDEQUE_H
#include <vector>

class ArrayDeque{
    
    public:
        ArrayDeque( int maxSize) ;
        ~ArrayDeque();
        bool isEmpty() const;
        bool isFull()const ;
        void insertFront(int element);
        void insertRear(int element);
        void deleteFront();
        void deleteRear();
        void print() const;
        int getFront()const;
        int getRear()const;
        
        static int mod(int a, int b);
    
private:
        int MAX_SIZE;
        std::vector<int> *data ;
        int front;
        int rear;
        int count;
};
#endif /* ARRAYDEQUE_H */
